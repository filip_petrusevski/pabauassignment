<?php
// include('db/patients.php');
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
include_once('classes/db.classes.php');
include('db/patients.php');
session_start();


?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">

    <title>Dashboard</title>
</head>

<body class="bg-white">
    <?php
    if (isset($_SESSION['message'])) {
    ?>
        <div class="alert alert-<?= $_SESSION['msg_type'] ?>">
            <?php
            echo $_SESSION['message'];
            unset($_SESSION['message']); ?>
        </div>
    <?php
    }
    ?>

    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="style/img/logo.png" alt="" width="24" height="24" class="d-inline-block align-text-top">
                Pabau
            </a>
            <ul class="navbar-nav">
                <li class="nav-item me-3">
                    <form action="includes/logout.php" method="POST">
                        <button class="button-style" type="submit" name="logout" href="includes/logout.php" class="nav-link">Log Out </button>
                    </form>

                </li>
            </ul>

        </div>
    </nav>
    <div class="containter d-flex">


        <div class="col-8 offset-2 mt-5">
            <?php
            while ($patients = $stmt->fetch(PDO::FETCH_ASSOC)) {
            ?>
                <div class="card my-3 " style="width: 18rem;">
                    <div class="card-header">
                        <?php echo "Name: " . $patients['name']; ?>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <?php echo "Id: " . $patients['id']; ?>
                        </li>
                        <li class="list-group-item">
                            <?php echo "Email: " . $patients['email']; ?>
                        </li>
                        <li class="list-group-item">
                            <?php echo "Phone Number: " . $patients['phone_number']; ?>
                        </li>
                        <li class="list-group-item">
                            <?php echo "Address: " . $patients['address']; ?>
                        </li>
                        <li class="list-group-item">
                            <?php echo "Medican condition: " . $patients['medical_condition']; ?>
                        </li>
                        <li class="list-group-item">
                            <?php echo "Blood Type:" . $patients['blood_type']; ?>
                        </li>
                        <li class="list-group-item text-center">
                            <a href="dashboard.php?edit=<?php echo $patients['id'] ?>" class="btn btn-info">Edit</a>
                            <a href="db/patients.php?delete=<?php echo $patients['id'] ?>" class="btn btn-danger">Delete</a>
                        </li>
                    </ul>
                </div>
            <?php
            }
            ?>




        </div>

    </div>







    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>

</html>