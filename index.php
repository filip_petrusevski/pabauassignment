<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// include('login.php');
include('db/db.php');


?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="style/style.css">

    <title>Login page</title>
</head>

<body class="d-flex justify-content-center h-100 align-middle align-items-center">
    <?php
    $db->login();
    ?>
    <form class="form-style shadow p-5 text-center" method="POST">
        <img class="logo" src="style/img/logo.png" alt="">
        <h2 class="blue-color mb-3">LOGIN</h2>
        <label class="blue-color" for="email">Email</label><br>
        <input id="email" type="email" name="email" :value="old('email')" required placeholder="email...">
        <br><br>
        <label class="blue-color" for="password">Password</label><br>
        <input id="password" type="password" name="password" required placeholder="password..."><br><br>
        <button id="login" class="button-style" name="submit" type="submit"">LOGIN</button>
    </form>

    <script src=" https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
            <script type="text/javascript">

            </script>

</body>

</html>