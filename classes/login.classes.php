<?php
class Login extends DB
{
    protected function getUser($email, $password)
    {
        $stmt =  $this->connect()->prepare("SELECT password FROM `patients` WHERE `email` = ? AND `password` = ?;");
        if (!$stmt->execute(array($email, $password))) {
            $stmt = null;
            header('Location: ../index.php?error=stmtfailed');
            exit();
        }
        $resultCheck = null;
        if ($stmt->rowCount() == 0) {
            $stmt = null;
            header("location: ../index.php?error=usernotfound");
            exit();
        }
        $pwd = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $checkPassword = password_verify($password, $pwd[0]["password"]);

        if ($checkPassword == false) {
            $stmt = null;
            header("location: ../index.php?error=wrongpassword");
            exit();
        } else if ($checkPassword == true) {
            $stmt =  $this->connect()->prepare('SELECT * FROM patients WHERE email = ? AND password = ?;');
            if (!$stmt->execute(array($email, $password))) {
                $stmt = null;
                header('Location: ../index.php?error=stmtfailed');
                exit();
            }
            if ($stmt->rowCount() == 0) {
                $stmt = null;
                header("location: ../index.php?error=usernotfound");
                exit();
            }
            $doctor = $stmt->fetchAll(PDO::FETCH_ASSOC);
            session_start();
            $_SESSION["doctorId"] = $doctor[0]["id"];

            $stmt = null;
        }
        $stmt = null;
    }
    protected function checkUser($email, $password)
    {
        $stmt =  $this->connect()->prepare('SELECT * FROM patients WHERE email = ? AND password = ?;');
        if (!$stmt->execute(array($email, $password))) {
            $stmt = null;
            header('Location: ../index.php?error=stmtfailed');
            exit();
        }
        $resultCheck = null;
        if ($stmt->rowCount() > 0) {
            $resultCheck = false;
        } else {
            $resultCheck = true;
        }
        return $resultCheck;
    }
}
