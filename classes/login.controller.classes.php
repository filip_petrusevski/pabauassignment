<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
class loginController extends Login
{
    private $email;
    private $password;
    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
    public function loginUser()
    {
        if ($this->emptyInput() == false) {
            header("location: ../index.php?error=emptyinput");
            exit();
        }
        $this->getUser($this->email, $this->password);
    }
    private function emptyInput()
    {
        $result = null;
        if (empty($this->email || empty($this->password))) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }
    // private function invalidEmail()
    // {
    //     $result = null;
    //     if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
    //         $result =  false;
    //     } else {
    //         $result = true;
    //     }
    //     return $result;
    // }
}
